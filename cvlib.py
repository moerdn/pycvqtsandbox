import cv, cv2

def DoCanny(image, thresh):

    """
    just try to do an canny edge detection
    """

    image = cv2.cvtColor(image, cv.CV_BGR2RGB)
    image = cv2.GaussianBlur(image, (9, 9),0)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = cv2.Canny(image, thresh, thresh*3, apertureSize=3)
    image = cv2.bitwise_and(image, image, mask = image)
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

    return image
