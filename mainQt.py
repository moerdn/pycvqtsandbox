#!/usr/bin/python

from sys import argv
import cv, cv2

from cvlib import DoCanny

from PyQt4 import QtGui

class Example(QtGui.QWidget):
    
    def __init__(self):
        super(Example, self).__init__()
        self.initUI()
        
    def initUI(self):      
        hbox = QtGui.QHBoxLayout(self)

        self.img_cv = cv2.imread("Image.jpg")
        height, width, depth = self.img_cv.shape

        self.img_cv = DoCanny(self.img_cv, 25)

        img_qt = QtGui.QImage(self.img_cv.data,
                              width,
                              height,
                              depth * width,
                              QtGui.QImage.Format_RGB888)

        pixmap = QtGui.QPixmap.fromImage(img_qt)

        lbl = QtGui.QLabel(self)
        lbl.setPixmap(pixmap)

        hbox.addWidget(lbl)
        self.setLayout(hbox)
        
        self.move(width, height)
        self.setWindowTitle('QtImage Viewer')
        self.show()        


if __name__ == "__main__":
    app = QtGui.QApplication(argv)
    ex = Example()
    app.exec_()
