#!/usr/bin/python

from sys import argv
from cv2 import imread, imshow, waitKey, destroyAllWindows

from cvlib import DoCanny

def main(argv):
    
    img = DoCanny(imread("Image.jpg"), 25)

    imshow("myWindow", img)

    # wait forever until keypress
    waitKey(0)



if __name__ == "__main__":
    main(argv)
